from conans import python_requires

common = python_requires('conan_common_recipes/0.0.1@Manu343726/testing')

class ExpectedLite(common.HeaderOnlyPackage):
    name = "expected-lite"
    url = "https://gitlab.com/Manu343726/conan-nonstdlite-packages"
    description = "Expected objects for C++11 and later (and later perhaps C++98)"
    version = "0.2.0"
    scm = {
        "type": "git",
        "subfolder": "expected-lite",
        "url": "https://github.com/martinmoene/expected-lite",
        "revision": "v" + version
    }

    pass
