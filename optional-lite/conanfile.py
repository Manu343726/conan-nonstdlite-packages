from conans import python_requires

common = python_requires('conan_common_recipes/0.0.1@Manu343726/testing')

class OptionalLite(common.HeaderOnlyPackage):
    name = "optional-lite"
    url = "https://gitlab.com/Manu343726/conan-nonstdlite-packages"
    description = "A single-file header-only version of a C++17-like optional, a nullable object for C++98, C++11 and later"
    version = "3.1.1"
    scm = {
        "type": "git",
        "subfolder": "optional-lite",
        "url": "https://github.com/martinmoene/optional-lite",
        "revision": "v" + version
    }

    pass
