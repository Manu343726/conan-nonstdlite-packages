from conans import python_requires

common = python_requires('conan_common_recipes/0.0.1@Manu343726/testing')

class ValuePtrLite(common.HeaderOnlyPackage):
    name = "value-ptr-lite"
    url = "https://gitlab.com/Manu343726/conan-nonstdlite-packages"
    description = "A C++ smart-pointer with value semantics for C++98, C++11 and later"
    version = "0.1.0"
    scm = {
        "type": "git",
        "subfolder": "value-ptr-lite",
        "url": "https://github.com/martinmoene/value-ptr-lite",
        "revision": "v" + version
    }

    pass
