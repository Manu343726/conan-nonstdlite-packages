from conans import python_requires

common = python_requires('conan_common_recipes/0.0.1@Manu343726/testing')

class VariantLite(common.HeaderOnlyPackage):
    name = "variant-lite"
    url = "https://gitlab.com/Manu343726/conan-nonstdlite-packages"
    description = "A single-file header-only version of a C++17-like variant, a type-safe union for C++98, C++11 and later"
    version = "1.1.0"
    scm = {
        "type": "git",
        "subfolder": "variant-lite",
        "url": "https://github.com/martinmoene/variant-lite",
        "revision": "v" + version
    }

    pass
